package com.dimox.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;

import com.dimox.bo.Noticia;

public class NoticiaTest extends JPAUnitTest {
	
	@Test
	public void entityManagerFactoryOK() {
		assertNotNull(emf);
	}
	
	@Test
	public void entityManagerOK() {
		assertNotNull(em);
	}
	
	@Test
	public void seleccionarNoticiaInicial() {
		Noticia noticia = em.find(Noticia.class, "java 9 ha salido");
		assertEquals("jose", noticia.getAutor());
	}
	
	@Test
	public void  borrarNoticiaInicial() {
		Noticia noticia = em.find(Noticia.class, "java 9 ha salido");
		em.getTransaction().begin();
		em.remove(noticia);
		em.getTransaction().commit();
		
		Noticia sinNoticia = em.find(Noticia.class, "java 9 ha salido");
		assertNull(sinNoticia);
		
	}
	
	@Test
	public void  insertarNuevaNoticia() {
		
		em.getTransaction().begin();
		Noticia noticia = new Noticia("Otra noticia nueva","Pedro", new Date());
		em.persist(noticia);
		em.getTransaction().commit();
		
		Noticia nuevaNoticia = em.find(Noticia.class, "Otra noticia nueva");
		assertNotNull(nuevaNoticia);
	}
	
}
