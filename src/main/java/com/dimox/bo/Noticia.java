package com.dimox.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Noticia implements Serializable {

	private static final long serialVersionUID = 1L;
	
		@Id
		private String titulo;
		private String autor;
		private Date fecha;
		
		@OneToMany(mappedBy="noticia", cascade=CascadeType.ALL)
		private List<Comentario> listComentarios = new ArrayList<Comentario>();
		
	
		// CONSTRUCTORES
		public Noticia() {
			super();
		}

		public Noticia(String titulo) {
			super();
			this.titulo = titulo;
		}

		public Noticia(String titulo, String autor, Date fecha) {
			super();
			this.titulo = titulo;
			this.autor = autor;
			this.fecha = fecha;
		}
		
		// SETTERS Y GETTERS
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public String getAutor() {
			return autor;
		}
		public void setAutor(String autor) {
			this.autor = autor;
		}
		public Date getFecha() {
			return fecha;
		}
		public void setFecha(Date fecha) {
			this.fecha = fecha;
		}
		
		public List<Comentario> getListComentarios() {
			return listComentarios;
		}

		public void setListComentarios(List<Comentario> listComentarios) {
			this.listComentarios = listComentarios;
		}

		// HASHCODE E EQUALS
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Noticia other = (Noticia) obj;
			if (titulo == null) {
				if (other.titulo != null)
					return false;
			} else if (!titulo.equals(other.titulo))
				return false;
			return true;
		}
		
		// METODOS
		
		public void addComentario(Comentario comentario) {
			listComentarios.add(comentario);
		}
		
}
